def drukuj_napis():
    tekst = "To jest mój napis."
    return tekst

def warunek(tf):
    if (tf == True):
        print("Prawda")
    elif(tf == False):
        print("Fałsz")
    else:
        print("Parametr nie jest zmienną logiczną True/False")

import math
def pole_i_obwod(r):
    p = math.pi * r * r
    ob = 2 * math.pi * r
    return p,ob
wynik = pole_i_obwod(5)
print(wynik)

def sprawdz_czy_lancuch(a):
    if isinstance(a,str) == True:
        print("Typ zmiennej: łańcuch.")
        czy_to_lancuch = True
    else:
        print("Typ zmiennej: nie jest to łańcuch.")
        czy_to_lancuch = False
    return czy_to_lancuch
b = "3"
wynik = sprawdz_czy_lancuch(b)
print(wynik)