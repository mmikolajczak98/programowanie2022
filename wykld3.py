#wyklad_3_241022
from re import A


przedmiot1 = 'programowanie'
przedmiot_2 = "statystyka"
#przedmiot 3 = "matematyka"

napis_1 = "uczeszczam na przedmiot 'matematyka'"
print(napis_1)

napis = "lista prowadzacych: \n 'Tomasz' \n 'Niedzielski'"
print(napis)

napis2 = "lista prowadzacych:" + "\n" + "Tomasz" + "\n" + "Niedzielski"
print(napis2)

liczba = "3"
liczba2 = '3'
liczba3 = 3

print(liczba3*10)
print(liczba2*10)

liczba4 = 3
liczba5 = 3.0
liczba6 = 3.5

print(liczba4 * 5)
print(liczba5 * 5)

typ = isinstance(liczba4,int)
print(typ)
typ2 = isinstance(liczba5,int)
print(typ2)

wartosc = liczba6.is_integer()
print(wartosc)

liczba7 = int(6.0)
print(liczba7 * 2)

liczba8 = float("4")
print(liczba8)

#lokalne_globalne
a = 5
print(a)
def wartosc():
    a = 7
    print(a)

wartosc()
print(a)

a=10
b=5
def pole(a,b):
    return(a*b)
print(pole(a,b))

import turtle