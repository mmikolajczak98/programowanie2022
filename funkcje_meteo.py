#importowanie bibliotek
import csv
import numpy as np
import os
#podprogram do generowania listy plików w dowolnym katalogu
def lista_plikow(katalog):
    lista = os.listdir(katalog)
    return lista

def czytaj_plik(sciezka_plik):
    pola = []
    lista = []
    with open(sciezka_plik, 'r', encoding="utf8") as plikcsv:
        csvreader = csv.reader(plikcsv)
        pola = next(csvreader)
        for wiersz in csvreader:
            lista.append(wiersz)
    tablica = np.array(lista)
    return tablica

#podprogram do obliczania minimalnej temperatury ze wszystkich stacji w jednym pomiarze
def temp_min(tablica):
    temperatura = []
    for i in range(0,len(tablica[:,4])):
        temperatura.append(float(tablica[i,4]))
    print(temperatura)
    t_min = min(temperatura)
    return t_min
print(temp_min(x))