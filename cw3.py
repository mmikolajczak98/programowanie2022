imie = "magda"
nazwisko = "mikolajczak"
print("Czesc %s. Nazywam się %s." %(imie, nazwisko))
print("Czesc {}. Nazywam sie {}." .format(imie, nazwisko))
print(f"Czesc {imie}. Nazywam sie {nazwisko}.")

import math
pi = math.pi
print(pi)
print("%.3f"%pi)
print("{:.3f}" .format(pi))
print(f"{pi:.3f}")

a = 3.2
b = 4.7
c = a+b
print(type(c))
print(isinstance(c, float))
print(isinstance(c, int))

if(isinstance(c, int)):
    print(f"Zmienna c jest typu int")
else:
    print(f"Zmienna c nie jest typu int")

import turtle
zolw = turtle.Turtle()
zolw.forward(100)
zolw.right(90)
zolw.forward(100)
zolw.right(90)
zolw.forward(100)
zolw.right(90)
zolw.forward(100)
turtle.exitonclick()
import turtle
zolw = turtle.Turtle()
import turtle
zolw = turtle.Turtle()
zolw.shape('turtle')
for i in range(10):
    zolw.circle(i*10)
turtle.exitonclick()