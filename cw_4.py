#zadanie_1_Wyświetl na ekranie długość zmiennej tekst, a następnie wyświetl na ekranie 13 znak ze zmiennej.
tekst = "to jest testowe zdanie"
print(len(tekst))
print(tekst[12])

#zadanie_2_Utworz w Pythonie listę zawierającą imiona: Jan, Ola. Napisz skrypt, który poprosi użytkownika o podanie imienia, a następie doda podane imię do listy i wyświetli całą listę na ekranie
listaImion =["Jan","Ola"]
imie = input("Podaj swoje imie: ")
listaImion.append(imie)
print(listaImion)

#zadanie_3_Na bazie listy z poprzedniego zadania wyświetl 3 element listy, oraz liczbę elementów które zawiera lista. Usuń z listy imię Jan.
listaImion2 = ["Jan", "Ola", "Tomej", 3.5, "Kasia", "Basia"]
print(listaImion2)
print(len(listaImion2))
print(listaImion2[2:])
print(listaImion2[:5])
print(listaImion2[5:6])

#zadanie_4 Utwórz tablice numpy z podanej listy lista = [2,3,5,6,4,8,11,43,23] oraz wyświetl na ekranie wartość maksymalna i średnią. Utwórz kopię tablicy, która będzie zawierała 10 krotnie większe wartości
import numpy as np
lista = [2,3,5,6,4,8,11,43,23]
tablica = np.array(lista)
print(lista)
print(tablica)
max = tablica.max()
srednia = tablica.mean()
print(f"wartosc max to: {max}")
print(f"wartosc srednia to: {srednia}")
kopiaTablicy = tablica*10
print(kopiaTablicy)
