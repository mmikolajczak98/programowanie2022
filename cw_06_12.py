import pandas as pd
import zipfile
import glob
import os
import re
zip = zipfile.ZipFile('guided_practice.zip')
zip.extractall("dane")
path = "./dane"                  
all_files = glob.glob(os.path.join(path, "*.csv")) 

dfs = list()
for i, f in enumerate(all_files):
    data = pd.read_csv(f)
    dfs.append(data)

df = pd.concat(dfs, ignore_index=True)

labels_to_print = ['stacja', 'data_pomiaru', 'godzina_pomiaru']

df
temp_max = df[df.temperatura == df.temperatura.max()]
temp_max[['stacja', 'data_pomiaru', 'godzina_pomiaru']]
temp_min = df[df.temperatura == df.temperatura.min()]
temp_min[['stacja', 'data_pomiaru', 'godzina_pomiaru']]
rainfall_present = df[df.suma_opadu > 0]
rainfall_present[['stacja', 'data_pomiaru', 'godzina_pomiaru']]