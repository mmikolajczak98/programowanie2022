import numpy as np
lista = [2, 7, 3]
tablica = np.array(lista)
print(lista)
print(tablica)

print(3*lista)
print(3*tablica)

w1 = np.array([2, 5, 8])
w2 = np.array([4, 1, 2])
print(w1)
print(w2)
print(w1+w2)
print(w1-w2)

lista_zagniezdzona = [[1, 2], [3, 4]]
print(lista_zagniezdzona)
macierz = np.array(lista_zagniezdzona)
print(macierz)
print(macierz[0,1])