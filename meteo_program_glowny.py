#importowanie bibliotek
import csv
import numpy as np
import os
from funkcje_meteo import lista_plikow, czytaj_plik, temp_min

#program do obliczania minimalnych temperatur w Polsce w kolejnych momentach obserwacji
moj_folder = "C:\\repozytorium\\programowanie2022-1\\"

#utworz liste plików z wykorzystaniem funkcji 'utworz_liste_plikow'
moja_lista = lista_plikow(moj_folder)

#pętla po plikach z listy - stosujemy funkcje "czytaj_jeden_plik" i "oblicz_min_temp"
min_temp_wektor = []
for plik in moja_lista:
    sciezka_plik = moj_folder + plik
    tablica_danych = czytaj_plik(sciezka_plik)
    min_temp = temp_min(tablica_danych)
    min_temp_wektor.append(min_temp)
print(min_temp_wektor)