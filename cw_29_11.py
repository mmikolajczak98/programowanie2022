#Wiem, że należało użyć csv reader, ale nijak nie mogłam sobie z tym poradzić, a w poradnikach
#najczęściej pojawiała się biblioteka pandas i tym sposobem już mi się udało uzyskać wyniki

import pandas as pd
#zad. 1
zmienna = "./meteo.csv"
plik = pd.read_csv(f"{zmienna}")
plik_naglowki = zmienna.columns.values.tolist()
print(plik_naglowki)
plik_lista = pd.read_csv(f"{zmienna}").values.tolist()
print(plik_lista)
tablica = pd.DataFrame(plik_lista, columns = plik_naglowki)
tablica
tablica.loc[3,:]

#zad. 2
srednia_temperatura = plik["temperatura"].mean()
srednia_wilgotnosc = plik["wilgotnosc_wzgledna"].mean()
srednie_cisnienie = plik["cisnienie"].mean()
print("Średnia temperatura: ", srednia_temperatura)
print("Średnia wilgotność: ", srednia_wilgotnosc)
print("Średnie ciśnienie: ", srednie_cisnienie)