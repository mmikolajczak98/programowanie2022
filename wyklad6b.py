#importowanie bibliotek
import csv
import numpy as np
import os
#podprogram do generowania listy plików w dowolnym katalogu
def lista_plikow(katalog):
    lista = os.listdir(katalog)
    return lista

def czytaj_plik(sciezka_plik):
    pola = []
    lista = []
    with open(sciezka_plik, 'r', encoding="utf8") as plikcsv:
        csvreader = csv.reader(plikcsv)
        pola = next(csvreader)
        for wiersz in csvreader:
            lista.append(wiersz)
    tablica = np.array(lista)
    return tablica
x = czytaj_plik("C:\\repozytorium\\programowanie2022-1\\meteo.csv")
print(x)

#podprogram do obliczania minimalnej temperatury ze wszystkich stacji w jednym pomiarze
def temp_min(tablica):
    temperatura = []
    for i in range(0,len(tablica[:,4])):
        temperatura.append(float(tablica[i,4]))
    print(temperatura)
    t_min = min(temperatura)
    return t_min
print(temp_min(x))

#program do obliczania minimalnych temperatur w Polsce w kolejnych momentach obserwacji
moj_folder = "C:\\repozytorium\\programowanie2022-1\\"

#utworz liste plików z wykorzystaniem funkcji 'utworz_liste_plikow'
moja_lista = lista_plikow(moj_folder)

#pętla po plikach z listy - stosujemy funkcje "czytaj_jeden_plik" i "oblicz_min_temp"
min_temp_wektor = []
for plik in moja_lista:
    sciezka_plik = moj_folder + plik
    tablica_danych = czytaj_plik(sciezka_plik)
    min_temp = temp_min(tablica_danych)
    min_temp_wektor.append(min_temp)
print(min_temp_wektor)